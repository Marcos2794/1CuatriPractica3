﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Practica3
{
    public partial class CajadeMensajes : Form
    {

        // Variable privada para el metodo Tipo de Boton
        private MessageBoxButtons TipoDeBoton = MessageBoxButtons.OK;

        // Variable privada para el metodo Tipo de Icono
        private MessageBoxIcon TipoIcono = MessageBoxIcon.Error;

        public CajadeMensajes()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void boton_CheckedChange(object sender, EventArgs e)
        {
            if (sender == rb_Ok)

                TipoDeBoton = MessageBoxButtons.OK;

            else if (sender == rb_ok_cancel)

                TipoDeBoton = MessageBoxButtons.OKCancel;

            else if (sender == rb_yes_no)

                TipoDeBoton = MessageBoxButtons.YesNo;

            else if (sender == rb_yes_no_cancel)

                TipoDeBoton = MessageBoxButtons.YesNoCancel;

            else if (sender == rb_retry_cancel)

                TipoDeBoton = MessageBoxButtons.RetryCancel;

            else

                TipoDeBoton = MessageBoxButtons.AbortRetryIgnore;


        }

        private void radioButton7_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void icono_CheckedChange(object sender, EventArgs e)
        {

            if (sender == rb_information)

                TipoIcono = MessageBoxIcon.Information;

            else if (sender == rb_exclamation)

                TipoIcono = MessageBoxIcon.Exclamation;

            else if (sender == rb_question)

                TipoIcono = MessageBoxIcon.Question;

            else

                TipoIcono = MessageBoxIcon.Error;

        }

        private void btn_Visualizar_Click(object sender, EventArgs e)
        {
            DialogResult result =
                MessageBox.Show("Mensaje a desplegar", "Titulo de la Ventana", TipoDeBoton, TipoIcono);
            switch (result)
            {
                case DialogResult.OK: lbl_mensaje.Text = "Selecciono Ok"; break;
                case DialogResult.Cancel: lbl_mensaje.Text = "Selecciono Cancel"; break;
                case DialogResult.Yes: lbl_mensaje.Text = "Selecciono Yes"; break;
                case DialogResult.No: lbl_mensaje.Text = "Selecciono No"; break;
                case DialogResult.Ignore: lbl_mensaje.Text = "Selecciono Ignore"; break;
                case DialogResult.Abort: lbl_mensaje.Text = "Selecciono Abort"; break;
                case DialogResult.Retry: lbl_mensaje.Text = "Selecciono Retry"; break;

            }
        }
    }
}

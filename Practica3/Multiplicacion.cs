﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Practica3
{
    public partial class Multiplicacion : Form
    {
        public Multiplicacion()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            double Num1;
            double Num2;
            double Respuesta;
            //Asi se extrae los volores del texbox
            Num1 = Convert.ToDouble(txt_Num1.Text);
            Num2 = Convert.ToDouble(txt_Num2.Text);
            Respuesta = Num1 * Num2;
            //Asi se muestran los volores en el texbox
            txt_Multiplicar.Text = string.Format("{0:F2}", Respuesta);

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            txt_Num1.Text = "";
            txt_Num2.Text = "";
            txt_Multiplicar.Text = "";
        }

        private void btn_salir_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}

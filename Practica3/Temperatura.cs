﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Practica3
{
    public partial class Temperatura : Form
    {
        public Temperatura()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void btn_CF_Click(object sender, EventArgs e)
            //ct1= convert to double
        {
            double gFarenheit;
            double ct1;

            ct1 = Convert.ToDouble(txt_IngreseTemperatura.Text);
            gFarenheit = ct1 * 1.8 + 32.0;
            txt_Resultado.Text = string.Format("{0:F3}", gFarenheit);


        }

        private void Temperatura_Load(object sender, EventArgs e)
        {

        }

        private void btn_FC_Click(object sender, EventArgs e)
        {
            double gCentigrados;
            double ct1;

            ct1 = Convert.ToDouble(txt_IngreseTemperatura.Text);
            gCentigrados = (ct1- 32.0)/1.8;
            txt_Resultado.Text = string.Format("{0:F3}", gCentigrados);
        }

        private void btn_Salir_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btn_Limpiar_Click(object sender, EventArgs e)
        {
            txt_IngreseTemperatura.Text = "";
            txt_Resultado.Text = "";
        }
    }
}

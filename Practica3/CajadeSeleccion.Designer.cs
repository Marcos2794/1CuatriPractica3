﻿namespace Practica3
{
    partial class CajadeSeleccion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.chk_Bold = new System.Windows.Forms.CheckBox();
            this.chk_italic = new System.Windows.Forms.CheckBox();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.chk_underline = new System.Windows.Forms.CheckBox();
            this.chk_Consolas = new System.Windows.Forms.CheckBox();
            this.chk_ColomnaMT = new System.Windows.Forms.CheckBox();
            this.chk_Verdana = new System.Windows.Forms.CheckBox();
            this.chk_Broadway = new System.Windows.Forms.CheckBox();
            this.chk_8 = new System.Windows.Forms.CheckBox();
            this.chk_12 = new System.Windows.Forms.CheckBox();
            this.chk_16 = new System.Windows.Forms.CheckBox();
            this.chk_20 = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(55, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(117, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "UTN POO  en C#";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(55, 99);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Estilo";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(194, 99);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "Funte";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(328, 99);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(60, 17);
            this.label4.TabIndex = 3;
            this.label4.Text = "Tamaño";
            // 
            // chk_Bold
            // 
            this.chk_Bold.AutoSize = true;
            this.chk_Bold.Font = new System.Drawing.Font("Consolas", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chk_Bold.Location = new System.Drawing.Point(58, 142);
            this.chk_Bold.Name = "chk_Bold";
            this.chk_Bold.Size = new System.Drawing.Size(62, 21);
            this.chk_Bold.TabIndex = 4;
            this.chk_Bold.Text = "Bold";
            this.chk_Bold.UseVisualStyleBackColor = true;
            // 
            // chk_italic
            // 
            this.chk_italic.AutoSize = true;
            this.chk_italic.Font = new System.Drawing.Font("Colonna MT", 10.2F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chk_italic.Location = new System.Drawing.Point(58, 169);
            this.chk_italic.Name = "chk_italic";
            this.chk_italic.Size = new System.Drawing.Size(67, 22);
            this.chk_italic.TabIndex = 5;
            this.chk_italic.Text = "Italic";
            this.chk_italic.UseVisualStyleBackColor = true;
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.Font = new System.Drawing.Font("Verdana", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox3.Location = new System.Drawing.Point(58, 201);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(104, 21);
            this.checkBox3.TabIndex = 6;
            this.checkBox3.Text = "StrickeOut";
            this.checkBox3.UseVisualStyleBackColor = true;
            // 
            // chk_underline
            // 
            this.chk_underline.AutoSize = true;
            this.chk_underline.Font = new System.Drawing.Font("Broadway", 7.8F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chk_underline.Location = new System.Drawing.Point(58, 228);
            this.chk_underline.Name = "chk_underline";
            this.chk_underline.Size = new System.Drawing.Size(106, 20);
            this.chk_underline.TabIndex = 7;
            this.chk_underline.Text = "Underline";
            this.chk_underline.UseVisualStyleBackColor = true;
            // 
            // chk_Consolas
            // 
            this.chk_Consolas.AutoSize = true;
            this.chk_Consolas.Font = new System.Drawing.Font("Consolas", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chk_Consolas.Location = new System.Drawing.Point(197, 142);
            this.chk_Consolas.Name = "chk_Consolas";
            this.chk_Consolas.Size = new System.Drawing.Size(94, 21);
            this.chk_Consolas.TabIndex = 8;
            this.chk_Consolas.Text = "Consolas";
            this.chk_Consolas.UseVisualStyleBackColor = true;
            // 
            // chk_ColomnaMT
            // 
            this.chk_ColomnaMT.AutoSize = true;
            this.chk_ColomnaMT.Font = new System.Drawing.Font("Colonna MT", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chk_ColomnaMT.Location = new System.Drawing.Point(197, 169);
            this.chk_ColomnaMT.Name = "chk_ColomnaMT";
            this.chk_ColomnaMT.Size = new System.Drawing.Size(103, 19);
            this.chk_ColomnaMT.TabIndex = 9;
            this.chk_ColomnaMT.Text = "Colomna MT";
            this.chk_ColomnaMT.UseVisualStyleBackColor = true;
            // 
            // chk_Verdana
            // 
            this.chk_Verdana.AutoSize = true;
            this.chk_Verdana.Font = new System.Drawing.Font("Verdana", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chk_Verdana.Location = new System.Drawing.Point(197, 196);
            this.chk_Verdana.Name = "chk_Verdana";
            this.chk_Verdana.Size = new System.Drawing.Size(87, 21);
            this.chk_Verdana.TabIndex = 10;
            this.chk_Verdana.Text = "Verdana";
            this.chk_Verdana.UseVisualStyleBackColor = true;
            // 
            // chk_Broadway
            // 
            this.chk_Broadway.AutoSize = true;
            this.chk_Broadway.Location = new System.Drawing.Point(197, 228);
            this.chk_Broadway.Name = "chk_Broadway";
            this.chk_Broadway.Size = new System.Drawing.Size(92, 21);
            this.chk_Broadway.TabIndex = 11;
            this.chk_Broadway.Text = "Broadway";
            this.chk_Broadway.UseVisualStyleBackColor = true;
            // 
            // chk_8
            // 
            this.chk_8.AutoSize = true;
            this.chk_8.Location = new System.Drawing.Point(331, 142);
            this.chk_8.Name = "chk_8";
            this.chk_8.Size = new System.Drawing.Size(38, 21);
            this.chk_8.TabIndex = 12;
            this.chk_8.Text = "8";
            this.chk_8.UseVisualStyleBackColor = true;
            // 
            // chk_12
            // 
            this.chk_12.AutoSize = true;
            this.chk_12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chk_12.Location = new System.Drawing.Point(331, 169);
            this.chk_12.Name = "chk_12";
            this.chk_12.Size = new System.Drawing.Size(56, 29);
            this.chk_12.TabIndex = 13;
            this.chk_12.Text = "12";
            this.chk_12.UseVisualStyleBackColor = true;
            // 
            // chk_16
            // 
            this.chk_16.AutoSize = true;
            this.chk_16.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chk_16.Location = new System.Drawing.Point(331, 200);
            this.chk_16.Name = "chk_16";
            this.chk_16.Size = new System.Drawing.Size(69, 36);
            this.chk_16.TabIndex = 14;
            this.chk_16.Text = "16";
            this.chk_16.UseVisualStyleBackColor = true;
            // 
            // chk_20
            // 
            this.chk_20.AutoSize = true;
            this.chk_20.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chk_20.Location = new System.Drawing.Point(331, 227);
            this.chk_20.Name = "chk_20";
            this.chk_20.Size = new System.Drawing.Size(77, 43);
            this.chk_20.TabIndex = 15;
            this.chk_20.Text = "20";
            this.chk_20.UseVisualStyleBackColor = true;
            // 
            // CajadeSeleccion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(704, 363);
            this.Controls.Add(this.chk_20);
            this.Controls.Add(this.chk_16);
            this.Controls.Add(this.chk_12);
            this.Controls.Add(this.chk_8);
            this.Controls.Add(this.chk_Broadway);
            this.Controls.Add(this.chk_Verdana);
            this.Controls.Add(this.chk_ColomnaMT);
            this.Controls.Add(this.chk_Consolas);
            this.Controls.Add(this.chk_underline);
            this.Controls.Add(this.checkBox3);
            this.Controls.Add(this.chk_italic);
            this.Controls.Add(this.chk_Bold);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "CajadeSeleccion";
            this.Text = "CajadeSeleccion";
            this.Load += new System.EventHandler(this.CajadeSeleccion_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox chk_Bold;
        private System.Windows.Forms.CheckBox chk_italic;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.CheckBox chk_underline;
        private System.Windows.Forms.CheckBox chk_Consolas;
        private System.Windows.Forms.CheckBox chk_ColomnaMT;
        private System.Windows.Forms.CheckBox chk_Verdana;
        private System.Windows.Forms.CheckBox chk_Broadway;
        private System.Windows.Forms.CheckBox chk_8;
        private System.Windows.Forms.CheckBox chk_12;
        private System.Windows.Forms.CheckBox chk_16;
        private System.Windows.Forms.CheckBox chk_20;
    }
}
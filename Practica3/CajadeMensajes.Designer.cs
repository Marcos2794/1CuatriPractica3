﻿namespace Practica3
{
    partial class CajadeMensajes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label18 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rb_abort_retry_cancel = new System.Windows.Forms.RadioButton();
            this.rb_retry_cancel = new System.Windows.Forms.RadioButton();
            this.rb_yes_no_cancel = new System.Windows.Forms.RadioButton();
            this.rb_ok_cancel = new System.Windows.Forms.RadioButton();
            this.rb_yes_no = new System.Windows.Forms.RadioButton();
            this.rb_Ok = new System.Windows.Forms.RadioButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.rb_error = new System.Windows.Forms.RadioButton();
            this.rb_question = new System.Windows.Forms.RadioButton();
            this.rb_exclamation = new System.Windows.Forms.RadioButton();
            this.rb_information = new System.Windows.Forms.RadioButton();
            this.btn_Visualizar = new System.Windows.Forms.Button();
            this.lbl_mensaje = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(69, 52);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(54, 17);
            this.label18.TabIndex = 17;
            this.label18.Text = "label18";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rb_abort_retry_cancel);
            this.groupBox1.Controls.Add(this.rb_retry_cancel);
            this.groupBox1.Controls.Add(this.rb_yes_no_cancel);
            this.groupBox1.Controls.Add(this.rb_ok_cancel);
            this.groupBox1.Controls.Add(this.rb_yes_no);
            this.groupBox1.Controls.Add(this.rb_Ok);
            this.groupBox1.Location = new System.Drawing.Point(33, 118);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(290, 287);
            this.groupBox1.TabIndex = 18;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Tipo de Boton";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // rb_abort_retry_cancel
            // 
            this.rb_abort_retry_cancel.AutoSize = true;
            this.rb_abort_retry_cancel.Location = new System.Drawing.Point(6, 252);
            this.rb_abort_retry_cancel.Name = "rb_abort_retry_cancel";
            this.rb_abort_retry_cancel.Size = new System.Drawing.Size(168, 21);
            this.rb_abort_retry_cancel.TabIndex = 5;
            this.rb_abort_retry_cancel.TabStop = true;
            this.rb_abort_retry_cancel.Text = "Abort   Retry    Cancel";
            this.rb_abort_retry_cancel.UseVisualStyleBackColor = true;
            this.rb_abort_retry_cancel.CheckedChanged += new System.EventHandler(this.boton_CheckedChange);
            // 
            // rb_retry_cancel
            // 
            this.rb_retry_cancel.AutoSize = true;
            this.rb_retry_cancel.Location = new System.Drawing.Point(6, 213);
            this.rb_retry_cancel.Name = "rb_retry_cancel";
            this.rb_retry_cancel.Size = new System.Drawing.Size(122, 21);
            this.rb_retry_cancel.TabIndex = 4;
            this.rb_retry_cancel.TabStop = true;
            this.rb_retry_cancel.Text = "Retry    Cancel";
            this.rb_retry_cancel.UseVisualStyleBackColor = true;
            this.rb_retry_cancel.CheckedChanged += new System.EventHandler(this.boton_CheckedChange);
            // 
            // rb_yes_no_cancel
            // 
            this.rb_yes_no_cancel.AutoSize = true;
            this.rb_yes_no_cancel.Location = new System.Drawing.Point(6, 169);
            this.rb_yes_no_cancel.Name = "rb_yes_no_cancel";
            this.rb_yes_no_cancel.Size = new System.Drawing.Size(138, 21);
            this.rb_yes_no_cancel.TabIndex = 3;
            this.rb_yes_no_cancel.TabStop = true;
            this.rb_yes_no_cancel.Text = "Yes   No   Cancel";
            this.rb_yes_no_cancel.UseVisualStyleBackColor = true;
            this.rb_yes_no_cancel.CheckedChanged += new System.EventHandler(this.boton_CheckedChange);
            // 
            // rb_ok_cancel
            // 
            this.rb_ok_cancel.AutoSize = true;
            this.rb_ok_cancel.Location = new System.Drawing.Point(6, 79);
            this.rb_ok_cancel.Name = "rb_ok_cancel";
            this.rb_ok_cancel.Size = new System.Drawing.Size(98, 21);
            this.rb_ok_cancel.TabIndex = 2;
            this.rb_ok_cancel.TabStop = true;
            this.rb_ok_cancel.Text = "Ok  Cancel";
            this.rb_ok_cancel.UseVisualStyleBackColor = true;
            this.rb_ok_cancel.CheckedChanged += new System.EventHandler(this.boton_CheckedChange);
            // 
            // rb_yes_no
            // 
            this.rb_yes_no.AutoSize = true;
            this.rb_yes_no.Location = new System.Drawing.Point(6, 124);
            this.rb_yes_no.Name = "rb_yes_no";
            this.rb_yes_no.Size = new System.Drawing.Size(87, 21);
            this.rb_yes_no.TabIndex = 1;
            this.rb_yes_no.TabStop = true;
            this.rb_yes_no.Text = "Yes    No";
            this.rb_yes_no.UseVisualStyleBackColor = true;
            this.rb_yes_no.CheckedChanged += new System.EventHandler(this.boton_CheckedChange);
            // 
            // rb_Ok
            // 
            this.rb_Ok.AutoSize = true;
            this.rb_Ok.Location = new System.Drawing.Point(6, 39);
            this.rb_Ok.Name = "rb_Ok";
            this.rb_Ok.Size = new System.Drawing.Size(47, 21);
            this.rb_Ok.TabIndex = 0;
            this.rb_Ok.TabStop = true;
            this.rb_Ok.Text = "Ok";
            this.rb_Ok.UseVisualStyleBackColor = true;
            this.rb_Ok.CheckedChanged += new System.EventHandler(this.boton_CheckedChange);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.rb_error);
            this.groupBox2.Controls.Add(this.rb_question);
            this.groupBox2.Controls.Add(this.rb_exclamation);
            this.groupBox2.Controls.Add(this.rb_information);
            this.groupBox2.Location = new System.Drawing.Point(367, 119);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(252, 201);
            this.groupBox2.TabIndex = 19;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Tipo de Icono";
            // 
            // rb_error
            // 
            this.rb_error.AutoSize = true;
            this.rb_error.Location = new System.Drawing.Point(21, 149);
            this.rb_error.Name = "rb_error";
            this.rb_error.Size = new System.Drawing.Size(61, 21);
            this.rb_error.TabIndex = 9;
            this.rb_error.TabStop = true;
            this.rb_error.Text = "Error";
            this.rb_error.UseVisualStyleBackColor = true;
            this.rb_error.CheckedChanged += new System.EventHandler(this.icono_CheckedChange);
            // 
            // rb_question
            // 
            this.rb_question.AutoSize = true;
            this.rb_question.Location = new System.Drawing.Point(21, 114);
            this.rb_question.Name = "rb_question";
            this.rb_question.Size = new System.Drawing.Size(86, 21);
            this.rb_question.TabIndex = 8;
            this.rb_question.TabStop = true;
            this.rb_question.Text = "Question";
            this.rb_question.UseVisualStyleBackColor = true;
            this.rb_question.CheckedChanged += new System.EventHandler(this.icono_CheckedChange);
            // 
            // rb_exclamation
            // 
            this.rb_exclamation.AutoSize = true;
            this.rb_exclamation.Location = new System.Drawing.Point(21, 78);
            this.rb_exclamation.Name = "rb_exclamation";
            this.rb_exclamation.Size = new System.Drawing.Size(104, 21);
            this.rb_exclamation.TabIndex = 7;
            this.rb_exclamation.TabStop = true;
            this.rb_exclamation.Text = "Exclamation";
            this.rb_exclamation.UseVisualStyleBackColor = true;
            this.rb_exclamation.CheckedChanged += new System.EventHandler(this.icono_CheckedChange);
            // 
            // rb_information
            // 
            this.rb_information.AutoSize = true;
            this.rb_information.Location = new System.Drawing.Point(21, 38);
            this.rb_information.Name = "rb_information";
            this.rb_information.Size = new System.Drawing.Size(99, 21);
            this.rb_information.TabIndex = 6;
            this.rb_information.TabStop = true;
            this.rb_information.Text = "Information";
            this.rb_information.UseVisualStyleBackColor = true;
            this.rb_information.CheckedChanged += new System.EventHandler(this.icono_CheckedChange);
            // 
            // btn_Visualizar
            // 
            this.btn_Visualizar.Location = new System.Drawing.Point(367, 361);
            this.btn_Visualizar.Name = "btn_Visualizar";
            this.btn_Visualizar.Size = new System.Drawing.Size(85, 29);
            this.btn_Visualizar.TabIndex = 20;
            this.btn_Visualizar.Text = "Visualizar";
            this.btn_Visualizar.UseVisualStyleBackColor = true;
            this.btn_Visualizar.Click += new System.EventHandler(this.btn_Visualizar_Click);
            // 
            // lbl_mensaje
            // 
            this.lbl_mensaje.AutoSize = true;
            this.lbl_mensaje.Location = new System.Drawing.Point(367, 415);
            this.lbl_mensaje.Name = "lbl_mensaje";
            this.lbl_mensaje.Size = new System.Drawing.Size(61, 17);
            this.lbl_mensaje.TabIndex = 21;
            this.lbl_mensaje.Text = "Mensaje";
            // 
            // CajadeMensajes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(662, 510);
            this.Controls.Add(this.lbl_mensaje);
            this.Controls.Add(this.btn_Visualizar);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label18);
            this.Name = "CajadeMensajes";
            this.Text = "CajadeMensajes";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btn_Visualizar;
        private System.Windows.Forms.RadioButton rb_abort_retry_cancel;
        private System.Windows.Forms.RadioButton rb_retry_cancel;
        private System.Windows.Forms.RadioButton rb_yes_no_cancel;
        private System.Windows.Forms.RadioButton rb_ok_cancel;
        private System.Windows.Forms.RadioButton rb_yes_no;
        private System.Windows.Forms.RadioButton rb_Ok;
        private System.Windows.Forms.RadioButton rb_error;
        private System.Windows.Forms.RadioButton rb_question;
        private System.Windows.Forms.RadioButton rb_exclamation;
        private System.Windows.Forms.RadioButton rb_information;
        private System.Windows.Forms.Label lbl_mensaje;
    }
}